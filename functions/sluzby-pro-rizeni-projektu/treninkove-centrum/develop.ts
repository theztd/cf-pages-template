//       TEST

const DOMAIN = "https://easyproject-cz-web.pages.dev";

const REDIRECTS = {
  "view=knowledge&amp;category_id=11": "/dokumentace/crm",
  "view=knowledge&amp;category_id=12": "/dokumentace/helpdesk-cz",
  "view=knowledge&amp;category_id=13": "/dokumentace/dalsi-moduly",
  "view=knowledge&amp;category_id=16": "/dokumentace/3-technicka-dokumentace",
  "view=knowledge&amp;category_id=17": "/dokumentace/4-okrajove-pripady",
  "view=knowledge&amp;category_id=18": "/dokumentace/rizeni-rizik",
  "view=knowledge&amp;category_id=19": "/dokumentace/zmeny-ve-verzich",
  "view=knowledge&amp;category_id=2": "/dokumentace/projektove-rizeni",
  "view=knowledge&amp;category_id=3": "/dokumentace/rizeni-prace",
  "view=knowledge&amp;category_id=4": "/dokumentace/rizeni-portfolia",
  "view=knowledge&amp;category_id=5": "/dokumentace/agilni-rizeni",
  "view=knowledge&amp;category_id=6": "/dokumentace/prehledove-stranky",
  "view=knowledge&amp;category_id=7": "/dokumentace/sablony-projektu",
  "view=knowledge&amp;category_id=8": "/dokumentace/2-pokrocile-doplnkove-funkce",
  "view=knowledge&amp;category_id=9": "/dokumentace/rizeni-zdroju",
  "view=knowledge_detail&amp;id=150&amp;category_id=13": "/dokumentace/clanek/myslenkove-mapy"
}



export async function onRequest(context) {
  // // Contents of context object
  // const {
  //   request, // same as existing Worker API
  //   env, // same as existing Worker API
  //   params, // if filename includes [id] or [[path]]
  //   waitUntil, // same as ctx.waitUntil in existing Worker API
  //   next, // used for middleware or to fetch assets
  //   data, // arbitrary space for passing data between middlewares
  // } = context;

  try {
    let target = REDIRECTS[context.request.url.split("?")[1]];
    
    if (target) {
      return Response.redirect(DOMAIN + target, 303);
    } else {
      throw 'Unable to find target'
    }

  } catch (error) {
    return new Response(error);
  }

}
