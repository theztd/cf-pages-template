
export const DOMAIN = "https://easyproject-cz-web.pages.dev";
export const REDIRECTS = {  
  "view=knowledge_detail&amp;id=2&amp;category_id=8": "/dokumentace/clanek/rizeni-dochazky",
  "view=knowledge_detail&amp;id=57&amp;category_id=8": "/dokumentace/clanek/polozky-v-projektovem-rozpoctu-a-prehledech",
  "view=knowledge_detail&amp;id=78&amp;category_id=1": "/dokumentace/clanek/pracovni-vykazy",
  "view=knowledge_detail&amp;id=139&amp;category_id=16": "/dokumentace/clanek/poznamky-k-verzi-10-14-x",

  "view=knowledge_detail&amp;id=21": "/dokumentace/clanek/autentizace",

  "view=knowledge_detail&amp;id=61&amp;category_id=8": "/dokumentace/clanek/ldap-autentifikace",

  "view=knowledge_detail&amp;id=20": "/dokumentace/clanek/ganttuv-diagram",

  "view=knowledge_detail&amp;id=76&amp;category_id=1": "/dokumentace/clanek/fond-pracovni-doby",

  "view=knowledge_detail&amp;id=11&amp;category_id=1": "/dokumentace/clanek/sablony-exportu",

  "view=knowledge_detail&amp;id=86&amp;category_id=1": "/dokumentace/clanek/archivace-a-odarchivace-projektu",

  "view=knowledge_detail&amp;id=41&amp;category_id=3": "/dokumentace/clanek/caldav-synchronizace-kalendare-easy-project-do-mobilniho-zarizeni",

  "view=knowledge_detail&amp;id=5&amp;category_id=8": "/dokumentace/clanek/helpdesk",

  "view=knowledge_detail&amp;id=76&amp;category_id=3": "/dokumentace/clanek/fond-pracovni-doby",

  "view=knowledge_detail&amp;id=82&amp;category_id=1": "/dokumentace/clanek/grafy-a-diagramy",

  "view=knowledge_detail&amp;id=80&amp;category_id=3": "/dokumentace/clanek/ical-synchronizace",

  "view=knowledge_detail&amp;id=35&amp;category_id=1": "/dokumentace/clanek/vytvoreni-pod-projektu-clenstvi-a-role",

  "view=knowledge_detail&amp;id=39&amp;category_id=1": "/dokumentace/clanek/agilni-nastenky-scrum-kanban",

  "view=knowledge_detail&amp;id=48&amp;category_id=1": "/dokumentace/clanek/analyza-dosazene-hodnoty",

  "view=knowledge_detail&amp;id=49&amp;category_id=8": "/dokumentace/clanek/synchronizace-kalendare-a-kontaktu-outlook-plugin",

  "view=knowledge_detail&amp;id=5&amp;category_id=73": "/dokumentace/clanek/helpdesk",

  "view=knowledge_detail&amp;id=58&amp;category_id=8": "/dokumentace/clanek/vytvoreni-noveho-projektu",

  "view=knowledge_detail&amp;id=43&amp;category_id=2": "/dokumentace/clanek/wbs-rozpad-projektu",

  "view=knowledge_detail&amp;id=5&amp;category_id=18": "/dokumentace/clanek/helpdesk",

  "view=knowledge_detail&amp;id=81&amp;category_id=8": "/dokumentace/clanek/rizeni-rizik",

  "view=knowledge_detail&amp;id=135&amp;category_id=19": "/dokumentace/clanek/poznamky-k-verzi-10-12-3",

  "view=knowledge_detail&amp;id=57&amp;category_id=25": "/dokumentace/clanek/polozky-v-projektovem-rozpoctu-a-prehledech",

  "view=knowledge_detail&amp;id=125&amp;category_id=19": "/dokumentace/clanek/poznamky-k-verzi-10-10-1",

  "view=knowledge_detail&amp;id=148&amp;category_id=19": "/dokumentace/clanek/poznamky-k-verzi-11-0-3",

  "view=knowledge_detail&amp;id=36&amp;category_id=1": "/dokumentace/clanek/jak-pouzivat-rss-feed-atom-link",

  "view=knowledge_detail&amp;id=4&amp;category_id=1": "/dokumentace/clanek/kontakty-v10",

  "view=knowledge_detail&amp;id=41&amp;category_id=8": "/dokumentace/clanek/caldav-synchronizace-kalendare-easy-project-do-mobilniho-zarizeni",

  "view=knowledge_detail&amp;id=44&amp;category_id=12": "/dokumentace/clanek/problem-s-nastavenim-helpdesku-pro-gmail",

  "view=knowledge_detail&amp;id=49&amp;category_id=12": "/dokumentace/clanek/synchronizace-kalendare-a-kontaktu-outlook-plugin",

  "view=knowledge_detail&amp;id=63&amp;category_id=8": "/dokumentace/clanek/meny",

  "view=knowledge_detail&amp;id=69&amp;category_id=16": "/dokumentace/clanek/url-prefix-sub-uri-konfigurace-od-verze-11-nepodporovano",

  "view=knowledge_detail&amp;id=115&amp;category_id=8": "/dokumentace/clanek/konfigurace-e-mailovych-oznameni-z-easy-projectu",

  "view=knowledge_detail&amp;id=124&amp;category_id=16": "/dokumentace/clanek/migrujte-z-postgresu-na-mysql",

  "view=knowledge_detail&amp;id=14&amp;category_id=11": "/dokumentace/clanek/crm",

  "view=knowledge_detail&amp;id=144&amp;category_id=19": "/dokumentace/clanek/upgradovani-na-verzi-11",

  "view=knowledge_detail&amp;id=39&amp;category_id=5": "/dokumentace/clanek/agilni-nastenky-scrum-kanban",

  "view=knowledge_detail&amp;id=5&amp;category_id=12": "/dokumentace/clanek/helpdesk",

  "view=knowledge_detail&amp;id=50&amp;category_id=13": "/dokumentace/clanek/testovaci-scenare",

  "view=knowledge_detail&amp;id=50&amp;category_id=8": "/dokumentace/clanek/testovaci-scenare",

  "view=knowledge_detail&amp;id=70&amp;category_id=8": "/dokumentace/clanek/easy-dms-system-spravy-dokumentu",

  "view=knowledge_detail&amp;id=1&amp;category_id=1": "/dokumentace/clanek/vystrahy-system-vcasneho-varovani",

  "view=knowledge_detail&amp;id=1&amp;category_id=3": "/dokumentace/clanek/vystrahy-system-vcasneho-varovani",

  "view=knowledge_detail&amp;id=10&amp;category_id=1": "/dokumentace/clanek/typy-ukolu",

  "view=knowledge_detail&amp;id=10&amp;category_id=3": "/dokumentace/clanek/typy-ukolu",

  "view=knowledge_detail&amp;id=100&amp;category_id=16": "/dokumentace/clanek/poznamky-k-verzi-10-3-3",

  "view=knowledge_detail&amp;id=100&amp;category_id=19": "/dokumentace/clanek/poznamky-k-verzi-10-3-3",

  "view=knowledge_detail&amp;id=101&amp;category_id=16": "/dokumentace/clanek/poznamky-k-verzi-10-3-2",

  "view=knowledge_detail&amp;id=101&amp;category_id=19": "/dokumentace/clanek/poznamky-k-verzi-10-3-2",

  "view=knowledge_detail&amp;id=102&amp;category_id=16": "/dokumentace/clanek/poznamky-k-verzi-10-4-1",

  "view=knowledge_detail&amp;id=102&amp;category_id=19": "/dokumentace/clanek/poznamky-k-verzi-10-4-1",

  "view=knowledge_detail&amp;id=103&amp;category_id=16": "/dokumentace/clanek/poznamky-k-verzi-10-4-2",

  "view=knowledge_detail&amp;id=103&amp;category_id=19": "/dokumentace/clanek/poznamky-k-verzi-10-4-2",

  "view=knowledge_detail&amp;id=104&amp;category_id=16": "/dokumentace/clanek/poznamky-k-verzi-10-4-3",

  "view=knowledge_detail&amp;id=104&amp;category_id=19": "/dokumentace/clanek/poznamky-k-verzi-10-4-3",

  "view=knowledge_detail&amp;id=105&amp;category_id=16": "/dokumentace/clanek/poznamky-k-verzi-10-4-4",

  "view=knowledge_detail&amp;id=105&amp;category_id=19": "/dokumentace/clanek/poznamky-k-verzi-10-4-4",

  "view=knowledge_detail&amp;id=106&amp;category_id=16": "/dokumentace/clanek/poznamky-k-verzi-10-5-1",

  "view=knowledge_detail&amp;id=106&amp;category_id=19": "/dokumentace/clanek/poznamky-k-verzi-10-5-1",

  "view=knowledge_detail&amp;id=107": "/dokumentace/clanek/poznamky-k-verzi-10-6-x-podzimni-edice",

  "view=knowledge_detail&amp;id=107&amp;category_id=16": "/dokumentace/clanek/poznamky-k-verzi-10-6-x-podzimni-edice",

  "view=knowledge_detail&amp;id=107&amp;category_id=19": "/dokumentace/clanek/poznamky-k-verzi-10-6-x-podzimni-edice",

  "view=knowledge_detail&amp;id=108&amp;category_id=16": "/dokumentace/clanek/poznamky-k-verzi-10-6-1",

  "view=knowledge_detail&amp;id=108&amp;category_id=19": "/dokumentace/clanek/poznamky-k-verzi-10-6-1",

  "view=knowledge_detail&amp;id=109&amp;category_id=1": "/dokumentace/clanek/rychla-editace-ukolu",

  "view=knowledge_detail&amp;id=109&amp;category_id=3": "/dokumentace/clanek/rychla-editace-ukolu",

  "view=knowledge_detail&amp;id=11&amp;category_id=3": "/dokumentace/clanek/sablony-exportu",

  "view=knowledge_detail&amp;id=110&amp;category_id=16": "/dokumentace/clanek/poznamky-k-verzi-10-6-2",

  "view=knowledge_detail&amp;id=110&amp;category_id=19": "/dokumentace/clanek/poznamky-k-verzi-10-6-2",

  "view=knowledge_detail&amp;id=111&amp;category_id=16": "/dokumentace/clanek/poznamky-k-verzi-10-7-x",

  "view=knowledge_detail&amp;id=111&amp;category_id=19": "/dokumentace/clanek/poznamky-k-verzi-10-7-x",

  "view=knowledge_detail&amp;id=112&amp;category_id=16": "/dokumentace/clanek/poznamky-k-verzi-10-6-3",

  "view=knowledge_detail&amp;id=112&amp;category_id=19": "/dokumentace/clanek/poznamky-k-verzi-10-6-3",

  "view=knowledge_detail&amp;id=113&amp;category_id=16": "/dokumentace/clanek/poznamky-k-verzi-10-7-1",

  "view=knowledge_detail&amp;id=113&amp;category_id=19": "/dokumentace/clanek/poznamky-k-verzi-10-7-1",

  "view=knowledge_detail&amp;id=114&amp;category_id=16": "/dokumentace/clanek/poznamky-k-verzi-10-7-2",

  "view=knowledge_detail&amp;id=114&amp;category_id=19": "/dokumentace/clanek/poznamky-k-verzi-10-7-2",

  "view=knowledge_detail&amp;id=115&amp;category_id=12": "/dokumentace/clanek/konfigurace-e-mailovych-oznameni-z-easy-projectu",

  "view=knowledge_detail&amp;id=116&amp;category_id=16": "/dokumentace/clanek/poznamky-k-verzi-10-7-3",

  "view=knowledge_detail&amp;id=116&amp;category_id=19": "/dokumentace/clanek/poznamky-k-verzi-10-7-3",

  "view=knowledge_detail&amp;id=117&amp;category_id=16": "/dokumentace/clanek/poznamky-k-verzi-10-8-x",

  "view=knowledge_detail&amp;id=117&amp;category_id=19": "/dokumentace/clanek/poznamky-k-verzi-10-8-x",

  "view=knowledge_detail&amp;id=118&amp;category_id=16": "/dokumentace/clanek/poznamky-k-verzi-10-8-1",

  "view=knowledge_detail&amp;id=118&amp;category_id=19": "/dokumentace/clanek/poznamky-k-verzi-10-8-1",

  "view=knowledge_detail&amp;id=119&amp;category_id=16": "/dokumentace/clanek/poznamky-k-verzi-10-9-x",

  "view=knowledge_detail&amp;id=119&amp;category_id=19": "/dokumentace/clanek/poznamky-k-verzi-10-9-x",

  "view=knowledge_detail&amp;id=12&amp;category_id=1": "/dokumentace/clanek/dokumenty",

  "view=knowledge_detail&amp;id=12&amp;category_id=3": "/dokumentace/clanek/dokumenty",

  "view=knowledge_detail&amp;id=120&amp;category_id=16": "/dokumentace/clanek/poznamky-k-verzi-10-8-2",

  "view=knowledge_detail&amp;id=120&amp;category_id=19": "/dokumentace/clanek/poznamky-k-verzi-10-8-2",

  "view=knowledge_detail&amp;id=121&amp;category_id=16": "/dokumentace/clanek/poznamky-k-verzi-10-9-1",

  "view=knowledge_detail&amp;id=121&amp;category_id=19": "/dokumentace/clanek/poznamky-k-verzi-10-9-1",

  "view=knowledge_detail&amp;id=122&amp;category_id=16": "/dokumentace/clanek/poznamky-k-verzi-10-9-2",

  "view=knowledge_detail&amp;id=122&amp;category_id=19": "/dokumentace/clanek/poznamky-k-verzi-10-9-2",

  "view=knowledge_detail&amp;id=123&amp;category_id=16": "/dokumentace/clanek/poznamky-k-verzi-10-10-x",

  "view=knowledge_detail&amp;id=123&amp;category_id=19": "/dokumentace/clanek/poznamky-k-verzi-10-10-x",

  "view=knowledge_detail&amp;id=125&amp;category_id=16": "/dokumentace/clanek/poznamky-k-verzi-10-10-1",

  "view=knowledge_detail&amp;id=126&amp;category_id=16": "/dokumentace/clanek/poznamky-k-verzi-10-10-2",

  "view=knowledge_detail&amp;id=126&amp;category_id=19": "/dokumentace/clanek/poznamky-k-verzi-10-10-2",

  "view=knowledge_detail&amp;id=127&amp;category_id=16": "/dokumentace/clanek/poznamky-k-verzi-10-10-3",

  "view=knowledge_detail&amp;id=127&amp;category_id=19": "/dokumentace/clanek/poznamky-k-verzi-10-10-3",

  "view=knowledge_detail&amp;id=128&amp;category_id=16": "/dokumentace/clanek/poznamky-k-verzi-10-11-x",

  "view=knowledge_detail&amp;id=128&amp;category_id=19": "/dokumentace/clanek/poznamky-k-verzi-10-11-x",

  "view=knowledge_detail&amp;id=129&amp;category_id=16": "/dokumentace/clanek/poznamky-k-verzi-10-11-1",

  "view=knowledge_detail&amp;id=129&amp;category_id=19": "/dokumentace/clanek/poznamky-k-verzi-10-11-1",

  "view=knowledge_detail&amp;id=13&amp;category_id=1": "/dokumentace/clanek/opakujici-se-ukoly",

  "view=knowledge_detail&amp;id=13&amp;category_id=2": "/dokumentace/clanek/opakujici-se-ukoly",

  "view=knowledge_detail&amp;id=13&amp;category_id=3": "/dokumentace/clanek/opakujici-se-ukoly",

  "view=knowledge_detail&amp;id=130&amp;category_id=16": "/dokumentace/clanek/poznamky-k-verzi-10-11-2",

  "view=knowledge_detail&amp;id=130&amp;category_id=19": "/dokumentace/clanek/poznamky-k-verzi-10-11-2",

  "view=knowledge_detail&amp;id=131&amp;category_id=16": "/dokumentace/clanek/poznamky-k-verzi-10-11-3",

  "view=knowledge_detail&amp;id=131&amp;category_id=19": "/dokumentace/clanek/poznamky-k-verzi-10-11-3",

  "view=knowledge_detail&amp;id=132&amp;category_id=16": "/dokumentace/clanek/poznamky-k-verzi-10-11-4",

  "view=knowledge_detail&amp;id=132&amp;category_id=19": "/dokumentace/clanek/poznamky-k-verzi-10-11-4",

  "view=knowledge_detail&amp;id=133&amp;category_id=16": "/dokumentace/clanek/poznamky-k-verzi-10-12-1",

  "view=knowledge_detail&amp;id=133&amp;category_id=19": "/dokumentace/clanek/poznamky-k-verzi-10-12-1",

  "view=knowledge_detail&amp;id=134&amp;category_id=16": "/dokumentace/clanek/poznamky-k-verzi-10-12-2",

  "view=knowledge_detail&amp;id=134&amp;category_id=19": "/dokumentace/clanek/poznamky-k-verzi-10-12-2",

  "view=knowledge_detail&amp;id=135&amp;category_id=16": "/dokumentace/clanek/poznamky-k-verzi-10-12-3",

  "view=knowledge_detail&amp;id=136&amp;category_id=16": "/dokumentace/clanek/poznamky-k-verzi-10-12-4",

  "view=knowledge_detail&amp;id=136&amp;category_id=19": "/dokumentace/clanek/poznamky-k-verzi-10-12-4",

  "view=knowledge_detail&amp;id=137&amp;category_id=16": "/dokumentace/clanek/poznamky-k-verzi-10-13-x",

  "view=knowledge_detail&amp;id=137&amp;category_id=19": "/dokumentace/clanek/poznamky-k-verzi-10-13-x",

  "view=knowledge_detail&amp;id=138&amp;category_id=16": "/dokumentace/clanek/poznamky-k-verzi-11-0",

  "view=knowledge_detail&amp;id=138&amp;category_id=19": "/dokumentace/clanek/poznamky-k-verzi-11-0",

  "view=knowledge_detail&amp;id=139&amp;category_id=19": "/dokumentace/clanek/poznamky-k-verzi-10-14-x",

  "view=knowledge_detail&amp;id=14&amp;category_id=8": "/dokumentace/clanek/crm",

  "view=knowledge_detail&amp;id=140&amp;category_id=16": "/dokumentace/clanek/poznamky-k-verzi-10-13-1",

  "view=knowledge_detail&amp;id=140&amp;category_id=19": "/dokumentace/clanek/poznamky-k-verzi-10-13-1",

  "view=knowledge_detail&amp;id=141&amp;category_id=16": "/dokumentace/clanek/poznamky-k-verzi-10-13-2",

  "view=knowledge_detail&amp;id=141&amp;category_id=19": "/dokumentace/clanek/poznamky-k-verzi-10-13-2",

  "view=knowledge_detail&amp;id=142&amp;category_id=16": "/dokumentace/clanek/poznamky-k-verzi-10-13-3",

  "view=knowledge_detail&amp;id=142&amp;category_id=19": "/dokumentace/clanek/poznamky-k-verzi-10-13-3",

  "view=knowledge_detail&amp;id=143&amp;category_id=16": "/dokumentace/clanek/poznamky-k-verzi-10-14-0",

  "view=knowledge_detail&amp;id=143&amp;category_id=19": "/dokumentace/clanek/poznamky-k-verzi-10-14-0",

  "view=knowledge_detail&amp;id=144&amp;category_id=16": "/dokumentace/clanek/upgradovani-na-verzi-11",

  "view=knowledge_detail&amp;id=145&amp;category_id=1": "/dokumentace/clanek/dynamicke-filtry",

  "view=knowledge_detail&amp;id=146&amp;category_id=16": "/dokumentace/clanek/poznamky-k-verzi-11-0-1",

  "view=knowledge_detail&amp;id=146&amp;category_id=19": "/dokumentace/clanek/poznamky-k-verzi-11-0-1",

  "view=knowledge_detail&amp;id=147&amp;category_id=16": "/dokumentace/clanek/poznamky-k-verzi-11-0-2",

  "view=knowledge_detail&amp;id=147&amp;category_id=19": "/dokumentace/clanek/poznamky-k-verzi-11-0-2",

  "view=knowledge_detail&amp;id=148&amp;category_id=16": "/dokumentace/clanek/poznamky-k-verzi-11-0-3",

  "view=knowledge_detail&amp;id=149&amp;category_id=16": "/dokumentace/clanek/easy-sso-saml-a-oauth-2-0-v-prekladu",

  "view=knowledge_detail&amp;id=15&amp;category_id=1": "/dokumentace/clanek/akcni-tlacitka",

  "view=knowledge_detail&amp;id=15&amp;category_id=3": "/dokumentace/clanek/akcni-tlacitka",

  "view=knowledge_detail&amp;id=16&amp;category_id=1": "/dokumentace/clanek/typy-uzivatelu",

  "view=knowledge_detail&amp;id=16&amp;category_id=3": "/dokumentace/clanek/typy-uzivatelu",

  "view=knowledge_detail&amp;id=17&amp;category_id=1": "/dokumentace/clanek/stavy-ukolu",

  "view=knowledge_detail&amp;id=17&amp;category_id=3": "/dokumentace/clanek/stavy-ukolu",

  "view=knowledge_detail&amp;id=18&amp;category_id=13": "/dokumentace/clanek/sprava-pozadavku",

  "view=knowledge_detail&amp;id=18&amp;category_id=8": "/dokumentace/clanek/sprava-pozadavku",

  "view=knowledge_detail&amp;id=19&amp;category_id=1": "/dokumentace/clanek/e-mailova-oznameni-o-projektu-a-aktivitach",

  "view=knowledge_detail&amp;id=19&amp;category_id=3": "/dokumentace/clanek/e-mailova-oznameni-o-projektu-a-aktivitach",

  "view=knowledge_detail&amp;id=2&amp;category_id=9": "/dokumentace/clanek/rizeni-dochazky",

  "view=knowledge_detail&amp;id=20&amp;category_id=1": "/dokumentace/clanek/ganttuv-diagram",

  "view=knowledge_detail&amp;id=20&amp;category_id=2": "/dokumentace/clanek/ganttuv-diagram",

  "view=knowledge_detail&amp;id=20&amp;category_id=4": "/dokumentace/clanek/ganttuv-diagram",

  "view=knowledge_detail&amp;id=21&amp;category_id=1": "/dokumentace/clanek/autentizace",

  "view=knowledge_detail&amp;id=21&amp;category_id=3": "/dokumentace/clanek/autentizace",

  "view=knowledge_detail&amp;id=22&amp;category_id=1": "/dokumentace/clanek/role-a-prava",

  "view=knowledge_detail&amp;id=22&amp;category_id=4": "/dokumentace/clanek/role-a-prava",

  "view=knowledge_detail&amp;id=23&amp;category_id=1": "/dokumentace/clanek/sledovani-ukolu",

  "view=knowledge_detail&amp;id=23&amp;category_id=3": "/dokumentace/clanek/sledovani-ukolu",

  "view=knowledge_detail&amp;id=24&amp;category_id=1": "/dokumentace/clanek/sledovani-casu",

  "view=knowledge_detail&amp;id=24&amp;category_id=3": "/dokumentace/clanek/sledovani-casu",

  "view=knowledge_detail&amp;id=25&amp;category_id=1": "/dokumentace/clanek/soukrome-komentare",

  "view=knowledge_detail&amp;id=25&amp;category_id=3": "/dokumentace/clanek/soukrome-komentare",

  "view=knowledge_detail&amp;id=26&amp;category_id=1": "/dokumentace/clanek/barevne-schema",

  "view=knowledge_detail&amp;id=26&amp;category_id=3": "/dokumentace/clanek/barevne-schema",

  "view=knowledge_detail&amp;id=27&amp;category_id=1": "/dokumentace/clanek/e-mailova-oznameni",

  "view=knowledge_detail&amp;id=27&amp;category_id=3": "/dokumentace/clanek/e-mailova-oznameni",

  "view=knowledge_detail&amp;id=28&amp;category_id=1": "/dokumentace/clanek/filtry-1-0",

  "view=knowledge_detail&amp;id=28&amp;category_id=3": "/dokumentace/clanek/filtry-1-0",

  "view=knowledge_detail&amp;id=29&amp;category_id=1": "/dokumentace/clanek/prehled-aktivit",

  "view=knowledge_detail&amp;id=29&amp;category_id=3": "/dokumentace/clanek/prehled-aktivit",

  "view=knowledge_detail&amp;id=3&amp;category_id=13": "/dokumentace/clanek/znalosti-dokumentacni-system",

  "view=knowledge_detail&amp;id=3&amp;category_id=8": "/dokumentace/clanek/znalosti-dokumentacni-system",

  "view=knowledge_detail&amp;id=30&amp;category_id=1": "/dokumentace/clanek/verejne-projekty",

  "view=knowledge_detail&amp;id=30&amp;category_id=3": "/dokumentace/clanek/verejne-projekty",

  "view=knowledge_detail&amp;id=31&amp;category_id=1": "/dokumentace/clanek/webdav-online-upravy",

  "view=knowledge_detail&amp;id=31&amp;category_id=3": "/dokumentace/clanek/webdav-online-upravy",

  "view=knowledge_detail&amp;id=32&amp;category_id=1": "/dokumentace/clanek/jak-pocitat-dokoncenost-projektu-dokonceni",

  "view=knowledge_detail&amp;id=32&amp;category_id=2": "/dokumentace/clanek/jak-pocitat-dokoncenost-projektu-dokonceni",

  "view=knowledge_detail&amp;id=33&amp;category_id=1": "/dokumentace/clanek/vlastni-stranky",

  "view=knowledge_detail&amp;id=33&amp;category_id=6": "/dokumentace/clanek/vlastni-stranky",

  "view=knowledge_detail&amp;id=34&amp;category_id=1": "/dokumentace/clanek/nastenka",

  "view=knowledge_detail&amp;id=34&amp;category_id=3": "/dokumentace/clanek/nastenka",

  "view=knowledge_detail&amp;id=35&amp;category_id=7": "/dokumentace/clanek/vytvoreni-pod-projektu-clenstvi-a-role",

  "view=knowledge_detail&amp;id=36&amp;category_id=3": "/dokumentace/clanek/jak-pouzivat-rss-feed-atom-link",

  "view=knowledge_detail&amp;id=37&amp;category_id=1": "/dokumentace/clanek/jak-pracovat-s-klientskou-zonou",

  "view=knowledge_detail&amp;id=37&amp;category_id=3": "/dokumentace/clanek/jak-pracovat-s-klientskou-zonou",

  "view=knowledge_detail&amp;id=38&amp;category_id=10": "/dokumentace/clanek/rozpocty",

  "view=knowledge_detail&amp;id=38&amp;category_id=8": "/dokumentace/clanek/rozpocty",

  "view=knowledge_detail&amp;id=4&amp;category_id=3": "/dokumentace/clanek/kontakty-v10",

  "view=knowledge_detail&amp;id=40&amp;category_id=8": "/dokumentace/clanek/vytizeni-zdroju",

  "view=knowledge_detail&amp;id=40&amp;category_id=9": "/dokumentace/clanek/vytizeni-zdroju",

  "view=knowledge_detail&amp;id=41&amp;category_id=1": "/dokumentace/clanek/caldav-synchronizace-kalendare-easy-project-do-mobilniho-zarizeni",

  "view=knowledge_detail&amp;id=42&amp;category_id=1": "/dokumentace/clanek/carddav-synchronizace-kontaktu-z-easy-project-na-mobilni-zarizeni",

  "view=knowledge_detail&amp;id=42&amp;category_id=3": "/dokumentace/clanek/carddav-synchronizace-kontaktu-z-easy-project-na-mobilni-zarizeni",

  "view=knowledge_detail&amp;id=43&amp;category_id=1": "/dokumentace/clanek/wbs-rozpad-projektu",

  "view=knowledge_detail&amp;id=44&amp;category_id=16": "/dokumentace/clanek/problem-s-nastavenim-helpdesku-pro-gmail",

  "view=knowledge_detail&amp;id=44&amp;category_id=8": "/dokumentace/clanek/problem-s-nastavenim-helpdesku-pro-gmail",

  "view=knowledge_detail&amp;id=45&amp;category_id=8": "/dokumentace/clanek/rizeni-dovednosti",

  "view=knowledge_detail&amp;id=46&amp;category_id=8": "/dokumentace/clanek/planovani-vytizeni-skupin",

  "view=knowledge_detail&amp;id=46&amp;category_id=9": "/dokumentace/clanek/planovani-vytizeni-skupin",

  "view=knowledge_detail&amp;id=47&amp;category_id=8": "/dokumentace/clanek/prehled-zdroju",

  "view=knowledge_detail&amp;id=47&amp;category_id=9": "/dokumentace/clanek/prehled-zdroju",

  "view=knowledge_detail&amp;id=48&amp;category_id=2": "/dokumentace/clanek/analyza-dosazene-hodnoty",

  "view=knowledge_detail&amp;id=49&amp;category_id=13": "/dokumentace/clanek/synchronizace-kalendare-a-kontaktu-outlook-plugin",

  "view=knowledge_detail&amp;id=51&amp;category_id=1": "/dokumentace/clanek/organizacni-struktura",

  "view=knowledge_detail&amp;id=53&amp;category_id=1": "/dokumentace/clanek/obchodni-panely",

  "view=knowledge_detail&amp;id=53&amp;category_id=6": "/dokumentace/clanek/obchodni-panely",

  "view=knowledge_detail&amp;id=54&amp;category_id=16": "/dokumentace/clanek/instalacni-manual-pro-serverove-reseni",

  "view=knowledge_detail&amp;id=55&amp;category_id=1": "/dokumentace/clanek/gdpr-moznosti",

  "view=knowledge_detail&amp;id=56&amp;category_id=1": "/dokumentace/clanek/kouzelne-tlacitko-alias-sablony-ukolu",

  "view=knowledge_detail&amp;id=56&amp;category_id=3": "/dokumentace/clanek/kouzelne-tlacitko-alias-sablony-ukolu",

  "view=knowledge_detail&amp;id=57&amp;category_id=10": "/dokumentace/clanek/polozky-v-projektovem-rozpoctu-a-prehledech",

  "view=knowledge_detail&amp;id=58&amp;category_id=1": "/dokumentace/clanek/vytvoreni-noveho-projektu",

  "view=knowledge_detail&amp;id=59&amp;category_id=17": "/dokumentace/clanek/registr-okrajovych-pripadu",

  "view=knowledge_detail&amp;id=6&amp;category_id=1": "/dokumentace/clanek/workflow",

  "view=knowledge_detail&amp;id=6&amp;category_id=3": "/dokumentace/clanek/workflow",

  "view=knowledge_detail&amp;id=60&amp;category_id=17": "/dokumentace/clanek/co-jsou-okrajove-pripady",

  "view=knowledge_detail&amp;id=61&amp;category_id=1": "/dokumentace/clanek/ldap-autentifikace",

  "view=knowledge_detail&amp;id=61&amp;category_id=16": "/dokumentace/clanek/ldap-autentifikace",

  "view=knowledge_detail&amp;id=62&amp;category_id=16": "/dokumentace/clanek/hardwarove-a-softwarove-pozadavky-na-serverove-reseni",

  "view=knowledge_detail&amp;id=63&amp;category_id=10": "/dokumentace/clanek/meny",

  "view=knowledge_detail&amp;id=65": "/dokumentace/clanek/planovac-scheduler",

  "view=knowledge_detail&amp;id=65&amp;category_id=1": "/dokumentace/clanek/planovac-scheduler",

  "view=knowledge_detail&amp;id=65&amp;category_id=3": "/dokumentace/clanek/planovac-scheduler",

  "view=knowledge_detail&amp;id=66": "/dokumentace/clanek/cbs-struktura-rozpadu-nakladu",

  "view=knowledge_detail&amp;id=66&amp;category_id=10": "/dokumentace/clanek/cbs-struktura-rozpadu-nakladu",

  "view=knowledge_detail&amp;id=66&amp;category_id=8": "/dokumentace/clanek/cbs-struktura-rozpadu-nakladu",

  "view=knowledge_detail&amp;id=67&amp;category_id=12": "/dokumentace/clanek/zpracovani-helpdeskovych-zprav",

  "view=knowledge_detail&amp;id=67&amp;category_id=8": "/dokumentace/clanek/zpracovani-helpdeskovych-zprav",

  "view=knowledge_detail&amp;id=7&amp;category_id=1": "/dokumentace/clanek/uzivatelska-pole",

  "view=knowledge_detail&amp;id=7&amp;category_id=3": "/dokumentace/clanek/uzivatelska-pole",

  "view=knowledge_detail&amp;id=70&amp;category_id=13": "/dokumentace/clanek/easy-dms-system-spravy-dokumentu",

  "view=knowledge_detail&amp;id=71&amp;category_id=8": "/dokumentace/clanek/diagramy",

  "view=knowledge_detail&amp;id=72&amp;category_id=1": "/dokumentace/clanek/distribuovane-ukoly",

  "view=knowledge_detail&amp;id=72&amp;category_id=3": "/dokumentace/clanek/distribuovane-ukoly",

  "view=knowledge_detail&amp;id=73&amp;category_id=8": "/dokumentace/clanek/ukolove-sekvence",

  "view=knowledge_detail&amp;id=74&amp;category_id=1": "/dokumentace/clanek/projektove-zpravy",

  "view=knowledge_detail&amp;id=75&amp;category_id=1": "/dokumentace/clanek/kalendar-a-planovac-schuzek-ukonceno-od-verze-11",

  "view=knowledge_detail&amp;id=75&amp;category_id=3": "/dokumentace/clanek/kalendar-a-planovac-schuzek-ukonceno-od-verze-11",

  "view=knowledge_detail&amp;id=76&amp;category_id=6": "/dokumentace/clanek/fond-pracovni-doby",

  "view=knowledge_detail&amp;id=77&amp;category_id=1": "/dokumentace/clanek/checklisty",

  "view=knowledge_detail&amp;id=78&amp;category_id=3": "/dokumentace/clanek/pracovni-vykazy",

  "view=knowledge_detail&amp;id=79&amp;category_id=1": "/dokumentace/clanek/planovac-schuzek-mistnosti",

  "view=knowledge_detail&amp;id=80&amp;category_id=1": "/dokumentace/clanek/ical-synchronizace",

  "view=knowledge_detail&amp;id=81&amp;category_id=18": "/dokumentace/clanek/rizeni-rizik",

  "view=knowledge_detail&amp;id=83&amp;category_id=1": "/dokumentace/clanek/viditelnost-projektu-a-opravneni",

  "view=knowledge_detail&amp;id=83&amp;category_id=2": "/dokumentace/clanek/viditelnost-projektu-a-opravneni",

  "view=knowledge_detail&amp;id=84&amp;category_id=16": "/dokumentace/clanek/poznamky-k-verzi-09-00",

  "view=knowledge_detail&amp;id=84&amp;category_id=19": "/dokumentace/clanek/poznamky-k-verzi-09-00",

  "view=knowledge_detail&amp;id=85&amp;category_id=1": "/dokumentace/clanek/jak-pridat-noveho-uzivatele",

  "view=knowledge_detail&amp;id=85&amp;category_id=3": "/dokumentace/clanek/jak-pridat-noveho-uzivatele",

  "view=knowledge_detail&amp;id=86&amp;category_id=7": "/dokumentace/clanek/archivace-a-odarchivace-projektu",

  "view=knowledge_detail&amp;id=87&amp;category_id=1": "/dokumentace/clanek/rychle-panovani-projektu",

  "view=knowledge_detail&amp;id=87&amp;category_id=2": "/dokumentace/clanek/rychle-panovani-projektu",

  "view=knowledge_detail&amp;id=89&amp;category_id=1": "/dokumentace/clanek/milniky",

  "view=knowledge_detail&amp;id=89&amp;category_id=2": "/dokumentace/clanek/milniky",

  "view=knowledge_detail&amp;id=9&amp;category_id=1": "/dokumentace/clanek/avatary-a-gravatary",

  "view=knowledge_detail&amp;id=9&amp;category_id=3": "/dokumentace/clanek/avatary-a-gravatary",

  "view=knowledge_detail&amp;id=90&amp;category_id=16": "/dokumentace/clanek/poznamky-k-verzi-09-01",

  "view=knowledge_detail&amp;id=90&amp;category_id=19": "/dokumentace/clanek/poznamky-k-verzi-09-01",

  "view=knowledge_detail&amp;id=91&amp;category_id=16": "/dokumentace/clanek/poznamky-k-verzi-10-0-x",

  "view=knowledge_detail&amp;id=91&amp;category_id=19": "/dokumentace/clanek/poznamky-k-verzi-10-0-x",

  "view=knowledge_detail&amp;id=92&amp;category_id=16": "/dokumentace/clanek/database-encryption-on-cloud",

  "view=knowledge_detail&amp;id=93&amp;category_id=1": "/dokumentace/clanek/moznosti-vyhledavani",

  "view=knowledge_detail&amp;id=94&amp;category_id=16": "/dokumentace/clanek/poznamky-k-verzi-10-1-x",

  "view=knowledge_detail&amp;id=94&amp;category_id=19": "/dokumentace/clanek/poznamky-k-verzi-10-1-x",

  "view=knowledge_detail&amp;id=95&amp;category_id=1": "/dokumentace/clanek/kategorie-ukolu",

  "view=knowledge_detail&amp;id=95&amp;category_id=3": "/dokumentace/clanek/kategorie-ukolu",

  "view=knowledge_detail&amp;id=96&amp;category_id=16": "/dokumentace/clanek/e-mailova-oznameni-z-cloudu",

  "view=knowledge_detail&amp;id=97&amp;category_id=16": "/dokumentace/clanek/poznamky-k-verzi-10-2-2",

  "view=knowledge_detail&amp;id=97&amp;category_id=19": "/dokumentace/clanek/poznamky-k-verzi-10-2-2",

  "view=knowledge_detail&amp;id=98&amp;category_id=16": "/dokumentace/clanek/poznamky-k-verzi-10-2-3",

  "view=knowledge_detail&amp;id=98&amp;category_id=19": "/dokumentace/clanek/poznamky-k-verzi-10-2-3",

  "view=knowledge_detail&amp;id=99&amp;category_id=16": "/dokumentace/clanek/poznamky-k-verzi-10-3-1",

  "view=knowledge_detail&amp;id=99&amp;category_id=19": "/dokumentace/clanek/poznamky-k-verzi-10-3-1",

  "view=knowledge&amp;category_id=1": "/dokumentace/1-easy-project",

  "view=knowledge&amp;category_id=10": "/dokumentace/rizeni-financi",

  "view=knowledge&amp;category_id=11": "/dokumentace/crm",

  "view=knowledge&amp;category_id=12": "/dokumentace/helpdesk-cz",

  "view=knowledge&amp;category_id=13": "/dokumentace/dalsi-moduly",

  "view=knowledge&amp;category_id=16": "/dokumentace/3-technicka-dokumentace",

  "view=knowledge&amp;category_id=17": "/dokumentace/4-okrajove-pripady",

  "view=knowledge&amp;category_id=18": "/dokumentace/rizeni-rizik",

  "view=knowledge&amp;category_id=19": "/dokumentace/zmeny-ve-verzich",

  "view=knowledge&amp;category_id=2": "/dokumentace/projektove-rizeni",

  "view=knowledge&amp;category_id=3": "/dokumentace/rizeni-prace",

  "view=knowledge&amp;category_id=4": "/dokumentace/rizeni-portfolia",

  "view=knowledge&amp;category_id=5": "/dokumentace/agilni-rizeni",

  "view=knowledge&amp;category_id=6": "/dokumentace/prehledove-stranky",

  "view=knowledge&amp;category_id=7": "/dokumentace/sablony-projektu",

  "view=knowledge&amp;category_id=8": "/dokumentace/2-pokrocile-doplnkove-funkce",

  "view=knowledge&amp;category_id=9": "/dokumentace/rizeni-zdroju",

  "view=knowledge_detail&amp;id=150&amp;category_id=13": "/dokumentace/clanek/myslenkove-mapy",

};