<h1>Vzorove zakladni webove stranky</h1>

Stránky narozdil od tezkopadneho wordpresu, joomly atp nevyzaduji zadne servery, databaze atp. Jsou o dost rychlejsi a odolnejsi vuci utokum hackera a jejich provozovani je mozne i v zdarma dostupnych CDN.

Web je plne verzovany, editace stranek podporuje markdown format a cela administrace probiha v obycejnem textovem editoru.


Zakladni stranky obsahujici:
 * **landing page**, ktera muze poslouzit jako vizitka, produktova stranka atp. Zklada se z komponent, jejihz obsah lze menit v adresari [./hugo/data/](./hugo/data/)
 * **jednoduchy blog** na ktery lze snadno pridavat clanky prostrednictvim adresare [./hugo/content/blog/](./hugo/content/blog/)
 * **kontaktni formular**


## Instalace

Web vyuziva sablonovaci system [https://gohugo.io/](Hugo), ten zle relativne snadno sprovoznit na mnoha platformach. Dostupne binarky jsou k dizpozici na adrese [https://github.com/gohugoio/hugo/releases](https://github.com/gohugoio/hugo/releases).

Spustitelny soubor je samozrejme potreba umistit do adresare v promene **$PATH**


## Spusteni

Pro lokalni testovani staci v adresari s webem (./hugo/), spustit prikaz **hugo server**

a nasledne otevrit prohlizec na adrese [http://localhost:1313](http://localhost:1313)



## Licence

This Repository is licensed under the [MIT](https://github.com/StaticMania/portio-hugo/blob/master/LICENSE) License
