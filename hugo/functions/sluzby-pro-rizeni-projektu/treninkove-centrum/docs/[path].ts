
export async function onRequest(context) {


  let ret = {};
  ret["url"] = context.request.url;
  ret["params"] = context.params;

  return new Response(JSON.stringify(ret));
}