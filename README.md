# Příklad správy vlastního webu pomocí gitlabu


## Lokalni vyvoj


* Inicializace submodulu
```bash
git submodule update --init --recursive
git pull --recurse-submodules
```


* Start webu lokalně
```bash
cd hugo
hugo serve
```


<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml):

```
image: alpine:latest

pages:
  stage: deploy
  script:
  - echo 'Nothing to do...'
  artifacts:
    paths:
    - public
  only:
  - master
```

